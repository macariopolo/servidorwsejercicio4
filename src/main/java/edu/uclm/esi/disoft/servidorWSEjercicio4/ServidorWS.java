package edu.uclm.esi.disoft.servidorWSEjercicio4;

import java.util.Enumeration;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import javax.websocket.OnMessage;
import javax.websocket.OnOpen;
import javax.websocket.Session;
import javax.websocket.server.ServerEndpoint;

@ServerEndpoint(value="/ServidorWS")
public class ServidorWS {
	private static ConcurrentHashMap<String, Session> sessions=new ConcurrentHashMap<>();
	
	@OnOpen
	public void onOpen(Session session) {
		Map<String, List<String>> mapa = session.getRequestParameterMap();
		List<String> parametros = mapa.get("user");
		sessions.put(parametros.get(0), session);
		session.getAsyncRemote().sendText("Hola, " + parametros.get(0) + ". El servidor te saluda");
	}
	
	@OnMessage
	public void onMessage(Session session, String mensaje) {
		Enumeration<Session> eSessions = sessions.elements();
		while (eSessions.hasMoreElements()) {
			Session destinatario=eSessions.nextElement();
			try {
				destinatario.getAsyncRemote().sendText(mensaje);
			}
			catch (Exception e) {
				sessions.remove(destinatario.getId());
			}
		}
	}
}
